import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PicturesComponent } from './pictures/pictures.component';

const routes: Routes = [
  {path:'', component: LoginComponent},
  {path:'pictures', component: PicturesComponent},
  {path:'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
