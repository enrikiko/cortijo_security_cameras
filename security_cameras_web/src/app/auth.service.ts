import { EventEmitter, Injectable, Output } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from './../environments/environment';

interface myData {
  success: boolean
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private status = false;
  private certain = false;

  @Output()
  statusEvent: EventEmitter<boolean> = new EventEmitter();

  constructor(private router: Router,
              private http: HttpClient) { };

  statusEventEmitter(){ return this.statusEvent }
  propagateStatus(status: boolean){ this.statusEvent.emit(status) }

  checkToken(tenant: string, user: string, token : any){
    const url = environment.apiUrl + "/login"
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*'
      })
    let object : any = {}
    object["user"] = user;
    object["token"] = token;
    object["type"] = "token";
    object["tenant"] = tenant;
    this.http.post<any>(url, object, { headers: headers }).subscribe( data =>
    {
      if(data.status==true){
        this.status = true
        this.propagateStatus(this.status)
      }
      else {
        this.status = false
        this.propagateStatus(this.status)
      }
    })
  }

  login(tenant: string, user: string, password: string ) {
    const url = environment.apiUrl + "/login"
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*'
      })
    let object : any = {}
    object["user"] = user;
    object["password"] = password;
    object["type"] = "password";
    object["tenant"] = tenant;
    this.http.post<any>(url, object, { headers: headers }).subscribe( data =>
    {
      let  token = data.token
      if(data.status==true){
        window.localStorage.setItem('token', token+"."+tenant+"."+user)
        window.localStorage.setItem('tenant', tenant)
        window.localStorage.setItem('user', user)
        this.status = true
        this.propagateStatus(this.status)
      }
      else {
        this.status = false
        this.propagateStatus(this.status)
      }
    })

}
}
