import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { PicturesComponent } from './pictures/pictures.component';
// import { AmplifyAuthenticatorModule } from '@aws-amplify/ui-angular';
// import { Amplify, Auth } from 'aws-amplify';
// import awsconfig from '../aws-exports';

// Amplify.configure(awsconfig);
// Auth.configure(awsconfig);


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PicturesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

