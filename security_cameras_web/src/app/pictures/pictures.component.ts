import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../environments/environment';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-index',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {

  model!: NgbDateStruct;
  date!: {year: number, month: number};
  year:any = null;
  month:any  = null;
  day:any  = null;
  years:any  = [];
  months:any  = [];
  days:any  = [];
  picture_list:any  = []
  camera:any =[]
  token : any = null
  tenant :any = null
  userName :any = null
  loginFail:    Boolean = false;
  loginSuccess: Boolean = false;

  private _camera_list = [];
  public get camera_list() {
    return this._camera_list;
  }
  public set camera_list(value) {
    this._camera_list = value;
  }

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private calendar: NgbCalendar,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.auth.statusEventEmitter().subscribe(status => this.changeLoginResult(status));
    this.checkToken()
    let date_ob = new Date();
    this.year = date_ob.getFullYear()
    for( let year = this.year; year > 0; year-- ){this.years.push(year)}
    this.month = date_ob.getMonth() + 1
    for( let month = 12; month > 0; month-- ){this.months.push(month)}
    this.day = date_ob.getDate()
    for( let day = 31; day > 0; day-- ){this.days.push(day)}
    this.getCameras()
  }

  changeLoginResult(status: any){
    if(status)
    {
      this.loginSuccess = true
      this.loginFail = false
      console.log("loginSuccess");
    }else{
      this.loginFail = true
      this.loginSuccess = false
      console.log("loginFail");
      this.router.navigate(['login'])
    }
  }

  search(){
    this.year = this.model.year;
    this.month  = this.model.month;
    this.day   = this.model.day;
    this.getCameras()
  }
  scrollup(){
    window.scrollTo({
    top: 0,
    behavior: 'smooth'
  })}
  scrolldown(){
    window.scrollTo({
    top: document.body.scrollHeight,
    behavior: 'smooth'
  })}
  logout(){
    window.localStorage.removeItem('tenant')
    window.localStorage.removeItem('user')
    window.localStorage.removeItem('token')
    this.checkToken()
  }

  checkToken(){
    console.log("Checking credentias")
    this.tenant = window.localStorage.getItem('tenant')
    this.userName = window.localStorage.getItem('user')
    this.token = window.localStorage.getItem('token')
    console.log(this.token)
    if(this.token != null){
      this.auth.checkToken(this.tenant,this.userName,this.token)
    }
    if(this.token == null){
      console.log("loginFail");
      this.router.navigate(['login'])
    }
  }

  delete(picture: any){
    // console.log(picture)
    // console.log(picture.Id)
    // console.log(this.picture_list)
    const picture_list_index = this.picture_list.indexOf(picture, 0);
    // console.log(picture_list_index)
    if (picture_list_index > -1) {
      this.picture_list.splice(picture_list_index, 1);
    }
    // console.log(this.picture_list)
    let url = "/delete"
    if (environment.cloud){
      url = environment.apiUrl + "/delete"
    }
    const body = {
      "Id": picture.Id
    }
    let token = window.localStorage.getItem('token')
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + token
      })
    this.http.post<any>(url, body, { headers: headers }).subscribe(data => {
      // console.log(data)
      this.getCameras()
    });
  }

  getCameras(){
    let url = "/cameras"
    if (environment.cloud){
      url = environment.apiUrl + "/cameras"
    }
    let token = window.localStorage.getItem('token')
    if(token != null){
      const headers = new HttpHeaders({
        'Authorization': 'Bearer ' + token
        })
    const cameraBody = {
      "year": String(this.year),
      "month": String(this.month),
      "day": String(this.day),
      // "user": window.localStorage.getItem('user'),
      // "tenant": window.localStorage.getItem('tenant')
    }
    this.http.post<any>(url, cameraBody, { headers: headers }).subscribe(data => {
        this.camera_list = data
        // console.log(this.camera_list)
      });
    }
  }

  searchPicture(camera: any){
      console.log(camera)
      this.camera=camera
      this.refreshPictureList()
  }

  refreshPictureList(){

    let url = "/index"
    if (environment.cloud){
      url = environment.apiUrl + "/index"
    }

    let token = window.localStorage.getItem('token')
    if(token != null){
      const headers = new HttpHeaders({
        'Authorization': 'Bearer ' + token
        })
      const body = {
        "year": String(this.year),
        "month": this.month,
        "day": this.day,
        "camera": this.camera,
        "user": window.localStorage.getItem('user'),
        "tenant": window.localStorage.getItem('tenant')
      }
      console.log(body)
      this.http.post<any>(url, body, { headers: headers }).subscribe(data => {
            this.picture_list = data
            console.log(this.picture_list)
          });
        }
    }

}
