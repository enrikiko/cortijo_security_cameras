import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  userName:     any  = "";
  tenant:     any  = "";
  password:     String  = "";
  loginFail:    Boolean = false;
  loginSuccess: Boolean = false;
  url                   = "assets/images/cortijo.png";
  tenantInvalid = false
  userInvalid = false
  passwordInvalid = false
  token : any = null


  constructor( 
    private router: Router,
    private auth: AuthService,
    private http: HttpClient ) { }

  ngOnInit() {
      this.auth.statusEventEmitter().subscribe(status => this.changeLoginResult(status));
      this.checkToken()
  }

  singIn(){
    this.router.navigate(['pictures'])
  }

  checkToken(){
    this.tenant = window.localStorage.getItem('tenant')
    this.userName = window.localStorage.getItem('user')
    this.token = window.localStorage.getItem('token')
    if(this.token != null && this.userName != null && this.tenant != null){
      this.auth.checkToken(this.tenant,this.userName,this.token)
    }
    
  }

  changeLoginResult(status: any){
    console.log("checking credentials")
    if(status)
    {
      this.loginSuccess = true
      this.loginFail = false
      this.singIn()
    }else{
      this.loginFail = true
      this.loginSuccess = false
      console.log("loginFail");
    }
  }

  checkBoxes(tenant: any, user: any, password: any){
    if(tenant==''){
      this.tenantInvalid=true
    }else{
      this.tenantInvalid=false 
    }
    if(user==''){
      this.userInvalid=true
    }else{
      this.userInvalid=false 
    }
    if(password==''){
      this.passwordInvalid=true
    }else{
      this.passwordInvalid=false 
    }
    if(!this.tenantInvalid&&!this.userInvalid&&!this.passwordInvalid){
      const loginResult = this.auth.login(tenant, user, password)
      console.log(loginResult)
    }
  }

  login(event: any){
    event.preventDefault()
    const target = event.target
    const tenant = target.querySelector('#tenant').value
    const user = target.querySelector('#userName').value
    const password = target.querySelector('#password').value
    this.checkBoxes(tenant,user,password)
  }
}
