export const environment = {
  production: true,
  cloud: true,
  apiUrl: 'https://cortijo-security-cameras-dev.cortijodemazas.com',
  env: 'test'
};
