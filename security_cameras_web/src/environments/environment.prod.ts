export const environment = {
  production: true,
  cloud: true,
  apiUrl: 'https://cortijo-security-cameras-prod.cortijodemazas.com',
  env: 'production'
};
