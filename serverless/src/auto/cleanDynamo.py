import json, boto3, os
import uuid
from botocore.exceptions import NoCredentialsError
from boto3.dynamodb.conditions import Key
from datetime import datetime
from datetime import timedelta
from boto3.dynamodb.conditions import Attr

AWS_REGION = os.environ['AWS_REGION']
DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']
EXPIRATION_DAYS = os.environ['EXPIRATION_DAYS']

def clean_dynamo_data(expiration_days):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMO_TABLE)
    data = datetime.now() - timedelta(days=7)
    timestamp = int(datetime.timestamp(data))
    print(timestamp)
    response = table.scan(
        FilterExpression=Attr('Timestamp').lte(timestamp) 
    )
    # print(response)
    itemsToDelete=response['Items']
    # print(itemsToDelete)
    for itemToDelete in itemsToDelete:
        print(itemToDelete)
        table.delete_item(Key={"Id": itemToDelete['Id']})


def lambda_handler(event, context):
    clean_dynamo_data(EXPIRATION_DAYS)