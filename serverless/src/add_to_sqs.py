import base64
import json, boto3, os, base64
import uuid
import base64
from botocore.exceptions import NoCredentialsError
from datetime import datetime

AWS_REGION = os.environ['AWS_REGION']
SQS_QUEUE = os.environ['SQS_QUEUE']

def get_pic_arn(event):
    # bucket = event['Records'][0]['s3']['bucket']['arn']
    path = event['Records'][0]['s3']['object']['key']
    # size = event['Records'][0]['s3']['object']['size']
    # pic = bucket + '/' + path
    return path

def sent_to_sqs(pic):
    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=SQS_QUEUE)
    queue.send_message(MessageBody=pic)

def lambda_handler(event, context):
    pic = get_pic_arn(event)
    sent_to_sqs(pic)