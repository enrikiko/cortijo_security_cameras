import json, boto3, os, base64
import uuid
from botocore.exceptions import NoCredentialsError
from datetime import datetime

BUCKET_NAME = os.environ['S3_BUCKET']
AWS_REGION = os.environ['AWS_REGION']
DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']



def AddZeroIfNeeded(val):
    if(int(val) < 10):
        result = "0"+str(int(val))
        return result
    else:
        return str(val)

def save_path_dynamo(path, camera, tenant):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMO_TABLE)
    data = datetime.now()
    tenant = str(tenant)
    timestamp = datetime.timestamp(data)
    second = AddZeroIfNeeded(str(data.second))
    minute = AddZeroIfNeeded(str(data.minute))
    hour = AddZeroIfNeeded(str(data.hour))
    day = AddZeroIfNeeded(str(data.day))
    month = AddZeroIfNeeded(str(data.month))
    year = str(data.year)
    id = str(uuid.uuid4())
    table.put_item(Item={'Tenant_Camera':tenant+'_'+camera,
                         'Id': id,
                         'Tenant': tenant,
                         'Year': year,
                         'Month': month,
                         'Day': day,
                         'Hour': hour,
                         'Minute': minute,
                         'Second': second,
                         'Timestamp': int(timestamp),
                         'Alert': False,
                         'Camera': camera,
                         'Path': str('https://' + BUCKET_NAME + '.s3-' + AWS_REGION + '.amazonaws.com/' + path)})
    return id

    
def upload_file(file, camera, tenant):
    global BUCKET_NAME
    s3 = boto3.client('s3')
    data = datetime.now()
    tenant = str(tenant)
    second = AddZeroIfNeeded(str(data.second))
    minute = AddZeroIfNeeded(str(data.minute))
    hour = AddZeroIfNeeded(str(data.hour))
    day = AddZeroIfNeeded(str(data.day))
    month = AddZeroIfNeeded(str(data.month))
    year = str(data.year)
    key = "captures/" + tenant + "/" + camera + "/" + year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + ".jpg"
    try:
        print(s3.put_object(Body=file, Bucket=BUCKET_NAME, Key=key))
        print(s3.put_object_acl(
            ACL='public-read',
            Bucket=BUCKET_NAME,
            Key=key,
        ))
        print("Upload Successful")
        return key
    except FileNotFoundError:
        print("The file was not found")
        return "Error"
    except NoCredentialsError:
        print("Credentials not available")
        return "Error"


def lambda_handler(event, context):
    camera = event['headers']['camera']
    tenant = event['headers']['tenant']
    b64_image = event['body']
    ascii_image = b64_image.encode('ascii')
    image = base64.b64decode(ascii_image)
    if image:
        s3_path = upload_file(image, camera, tenant)
        id = save_path_dynamo(s3_path, camera, tenant)
        id = {'Id': id}
        return {
            'statusCode': 200,
            'body': json.dumps(id)
        }
    else:
        return {
            'statusCode': 415,
            'body': 'Body is empty'
        }
