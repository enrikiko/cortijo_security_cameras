import json, boto3, os

from boto3.dynamodb.conditions import Attr

DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']


def get_index():
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMO_TABLE)
    response = table.scan(
        FilterExpression=Attr('Alert').eq(True)
    )
    return response["Items"]


def lambda_handler(event, context):
    index = get_index()
    return {
        'headers': {
            # 'Content-Type': 'application/json; charset=utf-8',
            'Access-Control-Allow-Origin': "*",
            # 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS'
        },
        'statusCode': 200,
        'body': json.dumps(index)
    }