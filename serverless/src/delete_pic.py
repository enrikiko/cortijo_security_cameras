import json, boto3, os, base64
from botocore.exceptions import NoCredentialsError

BUCKET_NAME = os.environ['S3_BUCKET']
AWS_REGION = os.environ['AWS_REGION']
DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']

def decode_file(encoded_file):
    decoded_file = base64.b64decode(encoded_file)
    return decoded_file

def delete_dynamo_item(id, tenant):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMO_TABLE)
    response = table.get_item(Key={'Tenant': tenant, 'Id': id})
    print(response['Item'])
    second = response['Item']['Second']
    day = response['Item']['Day']
    hour = response['Item']['Hour']
    minute = response['Item']['Minute']
    month = response['Item']['Month']
    year = response['Item']['Year']
    key = "captures/" + year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + ".jpg"
    table.delete_item( Key={
            'Tenant': tenant,
            'Id': id
        }
    )
    return key

def  delete_s3_file(s3_path):
    s3 = boto3.client('s3')
    try:
        print(s3.delete_object(Bucket=BUCKET_NAME, Key=s3_path))
        print("Deletion Successful")
    except FileNotFoundError:
        print("The file was not found")
        return "Error"
    except NoCredentialsError:
        print("Credentials not available")
        return "Error"


def lambda_handler(event, context):
    # print(event["body"])
    data = json.loads(event['body'])
    id = data['Id']
    # print(id)
    tenant = event['headers']['Authorization'].replace('Bearer ', '').split('.')[1]
    if id:
        s3_path = delete_dynamo_item(id, tenant)
        print(s3_path)
        delete_s3_file(s3_path)
        return {
            'headers': {
                'Access-Control-Allow-Origin': "*",
            },
            'statusCode': 200,
        }
    else:
        return {
            'headers': {
                'Access-Control-Allow-Origin': "*",
            },
            'statusCode': 415,
        }