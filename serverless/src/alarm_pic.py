import json, boto3, os, base64
from sqlite3 import Timestamp
import uuid
from botocore.exceptions import NoCredentialsError
from datetime import datetime

BUCKET_NAME = os.environ['S3_BUCKET']
AWS_REGION = os.environ['AWS_REGION']
DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']
SNS_ARN = os.environ['SNS_ARN']


def decode_file(encoded_file):
    decoded_file = base64.b64decode(encoded_file)
    return decoded_file


def send_notification(message):
    sns = boto3.client('sns', region_name='eu-central-1')
    sns_response = sns.publish(
        TargetArn=SNS_ARN,
        Message=json.dumps({'default': json.dumps(message)}),
        MessageStructure='json'
    )
    print(sns_response)


def save_path_dynamo(path):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMO_TABLE)
    data = datetime.now()
    timestamp = datetime.timestamp(data)
    second = str(data.second)
    minute = str(data.minute)
    hour = str(data.hour)
    day = str(data.day)
    month = str(data.month)
    year = str(data.year)
    table.put_item(Item={'Id': str(uuid.uuid4()),
                         'Year': year,
                         'Month': month,
                         'Day': day,
                         'Hour': hour,
                         'Minute': minute,
                         'Second': second,
                         'Timestamp': int(timestamp),
                         'Alert': True,
                         'Path': str('https://' + BUCKET_NAME + '.s3-' + AWS_REGION + '.amazonaws.com/' + path)})


def upload_file(file):
    global BUCKET_NAME
    s3 = boto3.client('s3')
    data = datetime.now()
    second = str(data.second)
    minute = str(data.minute)
    hour = str(data.hour)
    day = str(data.day)
    month = str(data.month)
    year = str(data.year)
    key = "alert/" + year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + ".jpg"
    try:
        print(s3.put_object(Body=file, Bucket=BUCKET_NAME, Key=key))
        print(s3.put_object_acl(
            ACL='public-read',
            Bucket=BUCKET_NAME,
            Key=key,
        ))
        print("Upload Successful")
        return key
    except FileNotFoundError:
        print("The file was not found")
        return "Error"
    except NoCredentialsError:
        print("Credentials not available")
        return "Error"


def lambda_handler(event, context):
    print(event["body"])
    data = json.loads(event['body'])
    pic = data['pic']
    if pic:
        s3_path = upload_file(decode_file(pic))
        save_path_dynamo(s3_path)
        location = boto3.client('s3').get_bucket_location(Bucket=BUCKET_NAME)['LocationConstraint']
        url = "https://%s.s3.%s.amazonaws.com/%s" % (BUCKET_NAME, location, s3_path)
        body = {"Message": "Alert, movement detected!", "url": url}
        send_notification(body)
        return {
            'statusCode': 200,
            'body': url
        }
    else:
        return {
            'statusCode': 415,
            'body': 'Body is empty'
        }
