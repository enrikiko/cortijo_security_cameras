import json, boto3, os
from boto3.dynamodb.conditions import Attr,Key

DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']
USERS_DYNAMO_TABLE = os.environ['DYNAMODB_USERS_TABLE']
client = boto3.resource('dynamodb')

def AddZeroIfNeeded(val):
    if(int(val) < 10):
        result = "0"+str(int(val))
        return result
    else:
        return str(val)


def get_picture(year, month, day, camera, tenant):
    client = boto3.resource('dynamodb')
    table = client.Table(DYNAMO_TABLE)
    response = table.query(
        KeyConditionExpression = Key('Tenant').eq(tenant)
    )
    data = response["Items"]
    newData = []
    data = sorted(data, key=lambda k: k['Timestamp'])
    for item in data:
        item['Timestamp'] = str(item['Timestamp'])
    if item['Year'] == year and item['Month'] == month and item['Day'] == day and item['Camera'] == camera :
        newData.append(item)
    return newData


def lambda_handler(event, context):
    data = json.loads(event['body'])
    tenant = event['headers']['Authorization'].replace('Bearer ', '').split('.')[1]
    year = data['year']
    month = AddZeroIfNeeded(data['month'])
    day = AddZeroIfNeeded(data['day'])
    camera = data['camera']
    index = get_picture(year, month, day, camera, tenant)
    return {
        'headers': {
            'Access-Control-Allow-Origin': "*",
        },
        'statusCode': 200,
        'body': json.dumps(index)
    }
