import json
import logging
import os
import time
import boto3
import uuid
from boto3.dynamodb.conditions import Key, Attr

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context):
    if (event['tenant'] is None) or (event['user'] is None) or (event['password'] is None):
        logging.error("Validation Failed")
        raise Exception("Couldn't create the todo item.")

    timestamp = str(time.time())
    user_token = str(uuid.uuid4())

    tenants_table = dynamodb.Table(os.environ['DYNAMODB_TENANTS_TABLE'])
    users_table = dynamodb.Table(os.environ['DYNAMODB_USERS_TABLE'])

    existing_tenant = tenants_table.query(
        KeyConditionExpression = Key('tenant').eq(str(event['tenant']))
    )

    existing_user = users_table.query(
        KeyConditionExpression = Key('tenant').eq(str(event['tenant'])) & Key('user').eq(str(event['user']))
    )

    if len(existing_tenant["Items"]) == 1:
        tenantExist = True
    else:
        tenantExist = False

    if existing_user["Count"] == 0:
        userDoesNotExist = True
    else:
        userDoesNotExist = False

    if tenantExist and userDoesNotExist:

        user = {
            'user': str(event['user']),
            'userName': str(event['user']),
            'password': str(event['password']),
            'tenant': str(event['tenant']),
            'createdAt': timestamp,
            'token': user_token,
        }

        users_table.put_item(Item=user)

        body = {
            'user': str(event['user']),
            "token": user_token,
            "user_created_successfully": True,
            "timestamp": timestamp
        }

        response = {
            "headers": {
                'Access-Control-Allow-Origin': "*",
            },
            "statusCode": 201,
            "body": json.dumps(body)
        }

    else:
        body = {
            "user_created_successfully": False,
            "error": 409
        }
        response = {
            "headers": {
                'Access-Control-Allow-Origin': "*",
            },
            "statusCode": 409,
            "body": json.dumps(body)
        }

    return response
