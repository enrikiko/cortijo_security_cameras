import json, boto3, os
from boto3.dynamodb.conditions import Attr,Key

DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']
USERS_DYNAMO_TABLE = os.environ['DYNAMODB_USERS_TABLE']
client = boto3.resource('dynamodb')


def AddZeroIfNeeded(val):
    if(int(val) < 10):
        result = "0"+str(int(val))
        return result
    else:
        return str(val)


def get_index(tenant, year, month, day):
    table = client.Table(DYNAMO_TABLE)
    response = table.query(
        KeyConditionExpression = Key('Tenant').eq(tenant)
    )
    data = response["Items"]
    print(data)
    newData = set()
    for item in data:
        if item['Year'] == year and item['Month'] == month and item['Day'] == day :
            newData.add(item['Camera'])
    dataList=list(newData)
    return dataList


def lambda_handler(event, context):
    print(event)
    data = json.loads(event['body'])
    token = event['headers']['Authorization'].replace('Bearer ', '')
    user_table = client.Table(USERS_DYNAMO_TABLE)
    existing_user = user_table.scan(
        FilterExpression=Attr('token').eq(token)
    )
    if len(existing_user['Items']) == 1:
        tenant = existing_user['Items'][0]['tenant']
    year = data['year']
    month = AddZeroIfNeeded(data['month'])
    day = AddZeroIfNeeded(data['day'])
    tenant = event['headers']['Authorization'].replace('Bearer ', '').split('.')[1]
    # print(tenant)
    index = get_index(tenant, year, month, day)
    return {
        'headers': {
            'Access-Control-Allow-Origin': "*",
        },
        'statusCode': 200,
        'body': json.dumps(index)
    }
