from __future__ import print_function
import os
import logging
import boto3
from boto3.dynamodb.conditions import Key, Attr

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context, tenant=None):
    if 'authorizationToken' not in event:
        logging.error("Validation Failed")
        raise Exception("Couldn't create the todo item.")
    token = event['authorizationToken'].replace('Bearer ', '').split('.')[0]
    tenant = event['authorizationToken'].replace('Bearer ', '').split('.')[1]
    user = event['authorizationToken'].replace('Bearer ', '').split('.')[2]
    print(token)
    users_table = dynamodb.Table(os.environ['DYNAMODB_USERS_TABLE'])
    existing_user = users_table.get_item(
        Key={'user': str(user), 'tenant': str(tenant)}
    )
    if int(existing_user["ResponseMetadata"]["HTTPHeaders"]["content-length"]) > 3:
        tenant = existing_user['Item']['tenant']
        # event['body']['tenant'] = tenant
        return generate_policy('Allow', tenant)
    else:
        return generate_policy('Deny', tenant)


def generate_policy(effect, tenant):
    return {
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [
                {
                    "Action": "execute-api:Invoke",
                    "Effect": effect,
                    "Resource": "*"
                }
            ]
        }
    }
