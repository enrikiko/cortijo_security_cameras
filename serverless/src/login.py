import json
import logging
import os
import time
import boto3
import uuid
from boto3.dynamodb.conditions import Key, Attr

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context):

    timestamp = str(time.time())

    data = json.loads(event['body'])
    if (data['tenant'] is None) or (data['user'] is None) or (data['type'] is None):
        logging.error("Validation Failed")
        raise Exception("Couldn't create the todo item.")
    
    tenants_table = dynamodb.Table(os.environ['DYNAMODB_TENANTS_TABLE'])
    users_table = dynamodb.Table(os.environ['DYNAMODB_USERS_TABLE'])

    existing_tenant = tenants_table.query(
        KeyConditionExpression = Key('tenant').eq(str(data['tenant']))
    )

    existing_user = users_table.get_item(
        Key={'user': str(data['user']), 'tenant': str(data['tenant'])}
    )
    print(existing_user)


    if existing_tenant["Count"] == 1:
        tenantExist = True
    else:
        tenantExist = False

    if int(existing_user["ResponseMetadata"]["HTTPHeaders"]["content-length"]) > 3:
        if existing_user["Item"]:
            userDoesExist = True
        else:
            userDoesExist = False
        
        if (data['type'] == "password"):
            if existing_user["Item"]:
                if(existing_user["Item"]["password"] == data['password']):
                    passwordCorrect = True
                else:
                    passwordCorrect = False
        
        if (data['type'] == "token"):
            if existing_user["Item"]:
                if(existing_user["Item"]["token"] == data['token'].split(".")[0]):
                    passwordCorrect = True
                else:
                    passwordCorrect = False
    else:
        userDoesExist = False
        passwordCorrect = False

    if tenantExist and userDoesExist and passwordCorrect:

        body = {
            "token": existing_user["Item"]["token"],
            "timestamp": timestamp,
            "status": True,
            "tenant": existing_user["Item"]["tenant"],
            "user": existing_user["Item"]["user"]
        }

        response = {
            "headers": {
                'Access-Control-Allow-Origin': "*",
            },
            "statusCode": 202,
            "body": json.dumps(body)
        }

    else:
        body = {
            "status": False,
            "error": 409
        }
        response = {
            "headers": {
                'Access-Control-Allow-Origin': "*",
            },
            "statusCode": 202,
            "body": json.dumps(body)
        }

    return response
