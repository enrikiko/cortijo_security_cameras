import json
import logging
import os
import time
import uuid
from boto3.dynamodb.conditions import Key, Attr
#

import boto3

dynamodb = boto3.resource('dynamodb')


def lambda_handler(event, context):
    if (event['tenant'] is None) or (event['user'] is None) or (event['password'] is None):
        logging.error("Validation Failed")
        raise Exception("Couldn't create the todo item.")

    timestamp = str(time.time())
    token = str(uuid.uuid4())
    user_token = str(uuid.uuid4())

    tenants_table = dynamodb.Table(os.environ['DYNAMODB_TENANTS_TABLE'])
    users_table = dynamodb.Table(os.environ['DYNAMODB_USERS_TABLE'])

    existing_tenant = tenants_table.query(
        KeyConditionExpression = Key('tenant').eq(str(event['tenant']))
    )

    if len(existing_tenant["Items"]) == 0:

        tenant = {
            'tenant': str(event['tenant']),
            'createdAt': timestamp,
            'token': token
        }

        user = {
            'user': str(event['user']),
            'userName': str(event['user']),
            'password': str(event['password']),
            'tenant': str(event['tenant']),
            'createdAt': timestamp,
            'token': user_token,
        }

        tenants_table.put_item(Item=tenant)
        users_table.put_item(Item=user)

        body = {
            "token": user_token,
            'tenant': str(event['tenant']),
            'user': str(event['user']),
            "timestamp": timestamp
        }

        response = {
            "statusCode": 201,
            "body": json.dumps(body)
        }

    else:
        body = {"error": 409}
        response = {
            "statusCode": 409,
            "body": json.dumps(body)
        }

    return response
