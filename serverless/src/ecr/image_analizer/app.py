import sys
import boto3
import os
import io
# from matplotlib import pyplot as plt
# from tensorflow import keras
# from mtcnn import MTCNN

AWS_REGION = os.environ['AWS_REGION']
BUCKET_NAME = os.environ['S3_BUCKET']

def handler(event, context):
    # getPic(event)
    # print(event)
    BUCKET_NAME=event['Records'][0]['s3']['bucket']['name']
    FILE_KEY=event['Records'][0]['s3']['object']['key']
    FILE_SIZE=event['Records'][0]['s3']['object']['size']
    print(BUCKET_NAME)
    print(FILE_KEY)
    print(FILE_SIZE)
    return 'Hello from AWS Lambda using Python' + sys.version + '!'     

# def getSQS(event):
#     messages = event['Records']
#     print("Number of Records")
#     print(len(event['Records']))
#     for message in messages:
#         pic_path=message['body']
#         getPic(pic_path)

# url='captures/2022-11-17-05-01-51.jpg'
# BUCKET_NAME='cortijo-security-cameras-dev'
# local_path='/tmp/test.jpg'
def getPic(url):
    s3 = boto3.client('s3')
    data_stream = io.BytesIO()
    print(BUCKET_NAME)
    print(url)
    data_stream = s3.get_object(
      Bucket=BUCKET_NAME, Key=url)["Body"].read()
    # s3.download_file(BUCKET_NAME, url, local_path)
    # print(data_stream)
    # image = plt.imread(local_path)
    # detector = MTCNN()
    # faces = detector.detect_faces(image)
    # print(faces)
    # for face in faces:
    #     print(face)m

#getPic(url)


#s3 = boto3.client('s3')
#data_stream = io.BytesIO()
#print(BUCKET_NAME)
#print(url)
#data_stream = s3.get_object(
#    Bucket=BUCKET_NAME, Key=url)["Body"].read()