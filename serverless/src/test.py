import base64
import json, boto3, os, base64
import uuid
import base64
from botocore.exceptions import NoCredentialsError
from datetime import datetime

BUCKET_NAME = os.environ['S3_BUCKET']
AWS_REGION = os.environ['AWS_REGION']
DYNAMO_TABLE = os.environ['DYNAMODB_INDEX_TABLE']

def lambda_handler(event, context):
    b64_image = event['body']
    image = base64.b64decode(b64_image)
    data = datetime.now()
    second = AddZeroIfNeeded(str(data.second))
    minute = AddZeroIfNeeded(str(data.minute))
    hour = AddZeroIfNeeded(str(data.hour))
    day = AddZeroIfNeeded(str(data.day))
    month = AddZeroIfNeeded(str(data.month))
    year = str(data.year)
    key = "captures/" + year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + ".jpg"
    s3 = boto3.client('s3')
    s3.put_object(Body= image, Bucket=BUCKET_NAME, Key=key)
    
    return {
        'statusCode': 200
    }