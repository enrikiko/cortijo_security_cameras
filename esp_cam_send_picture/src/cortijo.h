#ifndef CORTIJO_H /* include guards */
#define CORTIJO_H

void send_picture();
void send_wifi_signal();
void set_up();
bool is_movement();

#endif /* CORTIJO_H */