import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-index',
  templateUrl: './pictures.component.html',
  styleUrls: ['./pictures.component.css']
})
export class PicturesComponent implements OnInit {

  model: NgbDateStruct;
  date: {year: number, month: number};

  year = null;
  month = null;
  day = null;
  years = [];
  months = [];
  days = [];
  picture_list = []
  camera_list = []

  constructor(
    private http: HttpClient,
    private calendar: NgbCalendar
  ) { }

  ngOnInit(): void {
    let date_ob = new Date();
    this.year = date_ob.getFullYear()
    for( let year = this.year; year > 0; year-- ){this.years.push(year)}
    this.month = date_ob.getMonth() + 1
    for( let month = 12; month > 0; month-- ){this.months.push(month)}
    this.day = date_ob.getDate()
    for( let day = 31; day > 0; day-- ){this.days.push(day)}
    this.refreshPictureList()
    this.getCameras()
  }

  search(){
    this.year = this.model.year
    this.month  = this.model.month;
    this.day   = this.model.day;
    this.refreshPictureList()
  }

  delete(picture){
    // console.log(picture)
    // console.log(picture.Id)
    // console.log(this.picture_list)
    const picture_list_index = this.picture_list.indexOf(picture, 0);
    // console.log(picture_list_index)
    if (picture_list_index > -1) {
      this.picture_list.splice(picture_list_index, 1);
    }
    // console.log(this.picture_list)
    let url = "/delete"
    if (environment.cloud){
      url = environment.apiUrl + "/delete"
    }
    const body = {
      "Id": picture.Id
    }
    this.http.post<any>(url, body).subscribe(data => {
      // console.log(data)
      this.refreshPictureList()
    });
  }


  refreshPictureList(){
    let url = "/index"
    if (environment.cloud){
      url = environment.apiUrl + "/index"
    }

    const body = {
      "year": this.year,
      "month": this.month,
      "day": this.day
    }

    this.http.post<any>(url, body).subscribe(data => {
          this.picture_list = data
        });
    }

  getCameras(){
    let url = "/cameras"
    if (environment.cloud){
      url = environment.apiUrl + "/cameras"
    }

    const cameraBody = {
      "year": this.year,
      "month": this.month,
      "day": this.day
    }

    this.http.post<any>(url, cameraBody).subscribe(data => {
        this.camera_list = data
        console.log(this.camera_list)
      });
    }
}
