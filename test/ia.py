import requests
import base64
import cv2
import time
import imutils
camera = cv2.VideoCapture(0)
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
secret = True
while True:
    while secret==True:   
            return_value,image = camera.read()    
            gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)    
            # cv2.imshow('image',gray)    
            cv2.imwrite('test222.jpg',image)    
            secret = False
    image = cv2.imread('test222.jpg')
    image = imutils.resize(image,width=min(400, image.shape[1]))
    (regions, _) = hog.detectMultiScale(image, winStride=(4, 4), padding=(4, 4), scale=2.55)

    for (x, y, w, h) in regions:
        cv2.rectangle(image, (x, y), 
                  (x + w, y + h), 
                  (0, 0, 255), 2)
    cv2.imshow("Image", image)
    cv2.waitKey(0)
    secret = True
    cv2.destroyAllWindows()
    